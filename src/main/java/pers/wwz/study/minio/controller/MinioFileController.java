package pers.wwz.study.minio.controller;

import io.minio.*;
import io.minio.errors.*;
import io.minio.http.Method;
import org.apache.commons.lang3.StringUtils;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import pers.wwz.study.minio.constant.Constant;
import pers.wwz.study.minio.response.RestResult;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLEncoder;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 文件管理
 */
@RestController
@RequestMapping("/minioFile")
public class MinioFileController {

    @Resource
    private MinioClient minioClient;

    @Value("${minio.bucketName}")
    private String bucketName;

    /**
     * 文件上传
     * @param file
     * @return ResponseData
     */
    @PostMapping("/upload")
    public RestResult upload(@RequestParam(name = "file") MultipartFile[] file){
        if(file == null || file.length ==0){
            return RestResult.error("上传文件不能为空");
        }
        //防御性编程，批量限制少于10个
        if(file.length > Constant.FILE_UPLOAD_LIMIT_NUM){
            return RestResult.error("请上传少于" + file.length + "个文件");
        }
        try {
            List<String> urlList = new ArrayList<>(file.length);
            for (MultipartFile multipartFile : file){
                InputStream in = multipartFile.getInputStream();
//                minioClient.statObject(StatObjectArgs.builder().object(file.getOriginalFilename()).bucket(bucketName).build());
                minioClient.putObject(PutObjectArgs.builder().object(multipartFile.getOriginalFilename())
                        .bucket(bucketName)
                        .contentType(multipartFile.getContentType())
                        .stream(in, multipartFile.getSize(),-1).build());
                in.close();
                String str = minioClient.getPresignedObjectUrl(GetPresignedObjectUrlArgs.builder()
                        .bucket(bucketName)
                        .object(multipartFile.getOriginalFilename())
                        .method(Method.POST)
                        .build());

                String strSub = StringUtils.substringBefore(str, "?");
                urlList.add(strSub);
            }
            Map<String, Object> map = new HashMap<>();
            map.put("urlList", urlList);
            return RestResult.success(map.get("urlList"));
        } catch (Exception e) {
            return RestResult.error("上传失败:" + e.getMessage());
        }
    }


    /**
     * 下载文件
     * @param filename
     * @param response
     */
    @GetMapping("/download/{filename}")
    public void download(@PathVariable String filename, HttpServletResponse response){
        InputStream in = null;
        try {
            //获取对象信息
            StatObjectResponse stat = minioClient.statObject(
                    StatObjectArgs.builder().bucket(bucketName).object(filename).build());
            response.setContentType(stat.contentType());
            response.setHeader("Content-Disposition", "attachment;fileName=" + URLEncoder.encode(filename, "utf-8"));
            //文件下载
            in = minioClient.getObject(GetObjectArgs.builder().bucket(bucketName).object(filename).build());
            IOUtils.copy(in, response.getOutputStream());
        }catch (Exception e){
            e.getMessage();
        }finally {
            if(in != null){
                try {
                    in.close();
                }catch (IOException e){
                    e.getMessage();
                }
            }
        }
    }



    /**
     * 删除文件
     * @param filename
     */
    @PostMapping("/delete/{filename}")
    public void delete(@PathVariable String filename) throws ServerException, InsufficientDataException, ErrorResponseException, IOException, NoSuchAlgorithmException, InvalidKeyException, InvalidResponseException, XmlParserException, InternalException {
        minioClient.removeObject(RemoveObjectArgs.builder().bucket(bucketName).object(filename.toLowerCase()).build());
    }
}
