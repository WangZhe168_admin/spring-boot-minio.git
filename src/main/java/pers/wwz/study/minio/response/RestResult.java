package pers.wwz.study.minio.response;

import lombok.Data;

import static pers.wwz.study.minio.constant.Constant.*;

@Data
public class RestResult {

    private Boolean success;

    private String code;

    private String msg;

    private Object data;

    public static RestResult success(){
        RestResult restResult = new RestResult();
        restResult.setCode(SUCCESS_CODE);
        restResult.setMsg(SUCCESS_MSG);
        restResult.setSuccess(true);
        return restResult;
    }

    public static RestResult success(Object data){
        RestResult restResult = new RestResult();
        restResult.setCode(SUCCESS_CODE);
        restResult.setMsg(SUCCESS_MSG);
        restResult.setData(data);
        restResult.setSuccess(true);
        return restResult;
    }

    public static RestResult success(String msg, Object data){
        RestResult restResult = new RestResult();
        restResult.setCode(SUCCESS_CODE);
        restResult.setMsg(SUCCESS_MSG);
        restResult.setMsg(msg);
        restResult.setData(data);
        restResult.setSuccess(true);
        return restResult;
    }

    public static RestResult error(){
        RestResult restResult = new RestResult();
        restResult.setCode(ERROR_CODE);
        restResult.setMsg(ERROR_MSG);
        restResult.setSuccess(true);
        return restResult;
    }


    public static RestResult error(String msg){
        RestResult restResult = new RestResult();
        restResult.setCode(ERROR_CODE);
        restResult.setMsg(ERROR_MSG);
        restResult.setMsg(msg);
        restResult.setSuccess(false);
        return restResult;
    }
}
