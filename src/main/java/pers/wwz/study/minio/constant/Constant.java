package pers.wwz.study.minio.constant;

public class Constant {

    public static final String SUCCESS_CODE = "000000";

    public static final String SUCCESS_MSG = "success";

    public static final String ERROR_CODE = "E00001";

    public static final String ERROR_MSG = "error";

    public static final Integer FILE_UPLOAD_LIMIT_NUM = 10;
}
